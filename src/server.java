import java.io.*;
import java.net.*;
import javax.net.ServerSocketFactory;
import javax.net.ssl.*;
import java.security.KeyStore;
import javax.security.cert.X509Certificate;


public class server implements Runnable {
    private ServerSocket serverSocket = null;
    private static int numConnectedClients = 0;
    private String username;
    private String password;
    private userController uc = new userController();
    private TestLogger logger = new TestLogger();

    public server(ServerSocket ss) throws IOException {
        serverSocket = ss;
        newListener();
    }

    public void run() {
        try {
            SSLSocket socket = (SSLSocket) serverSocket.accept();
            newListener();
            SSLSession session = socket.getSession();
            X509Certificate cert = (X509Certificate) session.getPeerCertificateChain()[0];
            String subject = cert.getSubjectDN().getName();

            numConnectedClients++;
            System.out.println("client connected");
            System.out.println("client name (cert subject DN field): " + subject);
            System.out.println(numConnectedClients + " concurrent connection(s)\n");

            PrintWriter out = null;
            BufferedReader in = null;
            out = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));


            int loginAttempts = 0;
            int counter = 0;
            while (counter == 0) {
                username = in.readLine();
                password = in.readLine();
                String[] parts = uc.getRightUser(username);
                if (parts != null) {
                    loginAttempts = Integer.parseInt(parts[2]);
                }

                if (loginAttempts >= 5) {
                    logger.addToLog(username + "failed to login to many times");
                    out.println("this account is now locked an admin can unlock it for you");
                    out.flush();
                } else {
                    if (parts != null) {
                        if (uc.IsPasswordCorrect(username, password)) {
                            out.println("logged in");
                            logger.addToLog(username + " logged in");
                            counter++;
                            loginAttempts = 0;
                            uc.addloginAttempts(parts[6], String.valueOf(loginAttempts));
                            out.flush();
                        } else {
                            out.println("Wrong username or password");
                            out.flush();
                            loginAttempts++;
                            logger.addToLog(username + "failed to login");
                            uc.addloginAttempts(parts[6], String.valueOf(loginAttempts));
                        }
                    }else{
                        out.println("Wrong username or password");
                        out.flush();
                    }


                }
            }



            entity ent = uc.getSortOfEntity(username);


            ReferenceAccess ra = new ReferenceAccess();


            String clientMsg;
            while ((clientMsg = in.readLine()) != null) {
                String f = clientMsg.substring(0,1);
                String u = clientMsg.substring(2,5);
                switch (f) {
                    case "1":
                        String q = ra.readAccess(ent,u);
                        if (q.equals(" ")){
                            logger.addToLog(username + " failed to read medRecord");
                            out.println("You do not have access for this action");
                            out.flush();
                        }else{
                            logger.addToLog(username + " read patient medrecord");
                            out.println(q);
                            out.flush();
                        }

                        break;
                    case "2":
                        String c = clientMsg.substring(6,7);
                        String k = clientMsg.substring(8);
                        String l = ra.writeAccess(ent,u,c,k);
                        if (l.equals(" ")){
                            logger.addToLog(username + " failed to edit medRecord");
                            out.println("You do not have access for this action");
                            out.flush();
                        }else{
                            logger.addToLog(username + " edited patient medrecord");
                            out.println(l);
                            out.flush();
                        }

                        break;
                    case "3":
                        String y = clientMsg.substring(6);
                        if(ra.makeAccess(ent,u,y)){
                            logger.addToLog(username + " added medrecord");
                            out.println("Added successfully");
                            out.flush();
                        }else{
                            logger.addToLog(username + " failed to add medrecord");
                            out.println("You do not have access for this action");
                            out.flush();
                        }
                        break;
                    case "4":
                        String h = clientMsg.substring(6,7);
                        if (ra.deleteAccess(ent,u,h)){
                            logger.addToLog(username + " deleted medrecord");
                            out.println("Delete successful");
                            out.flush();
                        }else{
                            logger.addToLog(username + " failed to delete medrecord");
                            out.println("You do not have access for this action");
                            out.flush();
                        }

                    default:
                        break;
                }




            }





            in.close();
            out.close();
            socket.close();
            numConnectedClients--;
            System.out.println("client disconnected");
            System.out.println(numConnectedClients + " concurrent connection(s)\n");
        } catch (IOException e) {
            System.out.println("Client died: " + e.getMessage());
            e.printStackTrace();
            return;
        }
    }


    private void newListener() {
        (new Thread(this)).start();
    } // calls run()

    public static void main(String args[]) {
        System.out.println("\nServer Started\n");
        int port = 9876;
        if (args.length >= 1) {
            port = Integer.parseInt(args[0]);
        }
        String type = "TLS";
        try {
            ServerSocketFactory ssf = getServerSocketFactory(type);
            ServerSocket ss = ssf.createServerSocket(port);
            ((SSLServerSocket) ss).setNeedClientAuth(true); // enables client authentication
            new server(ss);
        } catch (IOException e) {
            System.out.println("Unable to start Server: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private static ServerSocketFactory getServerSocketFactory(String type) {
        if (type.equals("TLS")) {
            SSLServerSocketFactory ssf = null;
            try { // set up key manager to perform server authentication
                SSLContext ctx = SSLContext.getInstance("TLS");
                KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
                TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
                KeyStore ks = KeyStore.getInstance("JKS");
                KeyStore ts = KeyStore.getInstance("JKS");
                char[] password = "password".toCharArray();

                ks.load(new FileInputStream("src/serverkeystore"), password);  // keystore password (storepass)
                ts.load(new FileInputStream("src/servertruststore"), password); // truststore password (storepass)
                kmf.init(ks, password); // certificate password (keypass)
                tmf.init(ts);  // possible to use keystore as truststore here
                ctx.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
                ssf = ctx.getServerSocketFactory();
                return ssf;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            return ServerSocketFactory.getDefault();
        }
        return null;
    }
}

