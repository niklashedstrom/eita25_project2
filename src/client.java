import java.net.*;
import java.io.*;
import javax.net.ssl.*;
import javax.security.cert.X509Certificate;
import java.security.KeyStore;
import java.security.cert.*;
import java.util.Scanner;

/*
 * This example shows how to set up a key manager to perform client
 * authentication.
 *
 * This program assumes that the client is not inside a firewall.
 * The application can be modified to connect to a server outside
 * the firewall by following SSLSocketClientWithTunneling.java.
 */
public class client {

    public static void main(String[] args) throws Exception {
        String host = null;
        int port = -1;
        for (int i = 0; i < args.length; i++) {
            System.out.println("args[" + i + "] = " + args[i]);
        }
        if (args.length < 2) {
            System.out.println("USAGE: java client host port");
            System.exit(-1);
        }
        try { /* get input parameters */
            host = args[0];
            port = Integer.parseInt(args[1]);
        } catch (IllegalArgumentException e) {
            System.out.println("USAGE: java client host port");
            System.exit(-1);
        }

        try { /* set up a key manager for client authentication */
            SSLSocketFactory factory = null;
            try {
                char[] password = "password".toCharArray();
                KeyStore ks = KeyStore.getInstance("JKS");
                KeyStore ts = KeyStore.getInstance("JKS");
                KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
                TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
                SSLContext ctx = SSLContext.getInstance("TLS");
                Scanner sc = new Scanner(System.in);
                ks.load(new FileInputStream("clientkeystore"), password);  // keystore password (storepass)
                ts.load(new FileInputStream("clienttruststore"), password); // truststore password (storepass);
                kmf.init(ks, password); // user password (keypass)
                tmf.init(ts); // keystore can be used as truststore here
                ctx.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
                factory = ctx.getSocketFactory();

            } catch (Exception e) {
                throw new IOException(e.getMessage());
            }
            SSLSocket socket = (SSLSocket)factory.createSocket(host, port);
            System.out.println("\nsocket before handshake:\n" + socket + "\n");

            /*
             * send http request
             *
             * See SSLSocketClient.java for more information about why
             * there is a forced handshake here when using PrintWriters.
             */
            socket.startHandshake();

            SSLSession session = socket.getSession();
            X509Certificate cert = (X509Certificate)session.getPeerCertificateChain()[0];
            String issuer = cert.getIssuerDN().getName();
            String serial = cert.getSerialNumber().toString();
            String subject = cert.getSubjectDN().getName();
            System.out.println("certificate name (subject DN field) on certificate received from server:\n" + subject + "\n");
            System.out.println("certificate issuer (issuer DN field)\n" + issuer + "\n");
            System.out.println("Serialnumber of the Certificate:\n" + serial + "\n");
            System.out.println("socket after handshake:\n" + socket + "\n");
            System.out.println("secure connection established\n\n");

            BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String msg = null;
            String log = "logged in";
            while (!log.equals(msg)) {
            System.out.print("Username: ");
            String username = read.readLine();
            out.println(username);
            out.flush();
            Console console = System.console();
            if (console == null) {
                System.out.println("Couldn't get Console instance");
                System.exit(0);
            }
            char passwordArray[] = console.readPassword("Enter your secret password: ");
            out.println(passwordArray);
            out.flush();
            msg = in.readLine();
            System.out.println(msg);
            }
            System.out.println("Choose action");
            System.out.println("Press 1 + space + PATIENT_ID to read the patient's journal");
            System.out.println("Press 2 + space + PATIENT_ID + space + WHICH PATIENT JOURNAL, SELECT A NUMBER + space + TEXT  to edit a patient's journal");
            System.out.println("Press 3 + space + PATIENT_ID + space + TEXT to create a patient's journal");
            System.out.println("Press 4 + space + PATIENT_ID + space + WHICH PATIENT JOURNAL, SELECT A NUMBER to delete a patient's journal");




            String msg1 = read.readLine();
            while (msg1 != null) {
                System.out.print(">");
                msg1 = read.readLine();
                if (msg.equalsIgnoreCase("quit")) {
                    break;
                    }
                out.println(msg1);
                out.flush();
                System.out.println(in.readLine());

                System.out.println("Choose action");
                System.out.println("Press 1 + space + PATIENT_ID to read the patient's journal");
                System.out.println("Press 2 + space + PATIENT_ID + space + WHICH PATIENT JOURNAL, SELECT A NUMBER + space + TEXT  to edit a patient's journal");
                System.out.println("Press 3 + space + PATIENT_ID + space + TEXT to create a patient's journal");
                System.out.println("Press 4 + space + PATIENT_ID + space + WHICH PATIENT JOURNAL, SELECT A NUMBER to delete a patient's journal");
                msg1 = read.readLine();
            }



            in.close();
            out.close();
            read.close();
            socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
