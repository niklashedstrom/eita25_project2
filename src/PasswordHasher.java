import org.mindrot.jbcrypt.BCrypt;

public class PasswordHasher {

    private static int workload = 12;


    public static String hashPassword(String password_plaintext) {
        String salt = BCrypt.gensalt(workload);
        String hashed_password = BCrypt.hashpw(password_plaintext, salt);

        return(hashed_password);
    }

    public static boolean checkPassword(String password_plaintext, String stored_hash) {
        boolean password_verified = false;

        if(null == stored_hash || !stored_hash.startsWith("$2a$"))
            throw new java.lang.IllegalArgumentException("Invalid hash provided for comparison");

        password_verified = BCrypt.checkpw(password_plaintext, stored_hash);

        return(password_verified);
    }

    public static void main(String[] args) {
        String test_passwd = "abcdefghijklmnopqrstuvwxyz";
        String test_hash = "$2a$06$.rCVZVOThsIa97pEDOxvGuRRgzG64bvtJ0938xuqzv18d3ZpQhstC";

        System.out.println("Testing BCrypt Password hashing and verification");
        System.out.println("Test password: " + test_passwd);
        System.out.println("Test stored hash: " + test_hash);
        System.out.println("Hashing test password...");
        System.out.println();

        String computed_hash = hashPassword(test_passwd);
        System.out.println("Test computed hash: " + computed_hash);
        System.out.println();
        System.out.println("Verifying that hash and stored hash both match for the test password...");
        System.out.println();

        String compare_test = checkPassword(test_passwd, test_hash)
                ? "Passwords Match" : "Passwords do not match";
        String compare_computed = checkPassword(test_passwd, computed_hash)
                ? "Passwords Match" : "Passwords do not match";

        System.out.println("Verify against stored hash:   " + compare_test);
        System.out.println("Verify against computed hash: " + compare_computed);





        System.out.println("\nPatient passwords");
        System.out.println(hashPassword("Cheese1!"));
        System.out.println(hashPassword("Cracker2="));
        System.out.println(hashPassword("Grapes3#"));
        System.out.println(hashPassword("Knife4§"));
        System.out.println(hashPassword("Pear5%"));
        System.out.println(hashPassword("Brie6&"));
        System.out.println(hashPassword("Marmelade7/"));

        System.out.println("\n Nurse passwords");
        System.out.println(hashPassword("Overtime1!"));
        System.out.println(hashPassword("Shortage2="));
        System.out.println(hashPassword("Badpay3#"));

        System.out.println("\n Doctor Passwords");
        System.out.println(hashPassword("Goodpay1!"));
        System.out.println(hashPassword("Impressive2="));
        System.out.println(hashPassword("Wow3#"));

        System.out.println("\n Government Password");
        System.out.print(hashPassword("Kanejbytas123"));


        if(checkPassword("Cheese1!","$2a$12$QFO5CIu19hmqfx3D8XQmVedegCBF06PFlEeSoVvwQWRsw6HuCsE.6")){
            System.out.println("\nSuccess!");
        }
        else{

        }
    }

}

