import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;


public class userController {


    private HashMap<String, String> userNamePassword;
    File f = new File("src/EntityAccounts");
    ArrayList<String[]> all;
    private BufferedReader br;
    private ArrayList<ArrayList> entityData;

    public userController() throws IOException {
        entityData = new ArrayList<>();
        userNamePassword = new HashMap<>();
        all = new ArrayList<>();
        getAccounts();
        getEntityData();
    }

    private void getAccounts() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(f));
        String line = null;
        String username = null;
        String password = null;
        br.readLine();
        while (null != (line = br.readLine())) {
            username = line.substring(0, line.indexOf(":"));
            String temp = line.substring(line.indexOf(":") + 1);
            password = temp.substring(0, temp.indexOf(":"));
            if (username != null && password != null) {
                userNamePassword.put(username, password);

            }

        }

    }
    private void getEntityData() throws IOException {
        br = new BufferedReader(new FileReader(f));
        String all;
        br.readLine();
        while ((all = br.readLine()) != null) {
            ArrayList<String> strings = new ArrayList<>();
            String[] parts = all.split(":");
            for (int p = 0; p < parts.length; p++) {
                strings.add(parts[p]);
            }
            entityData.add(strings);
        }
    }
    public void addloginAttempts(String id,String attempts)throws FileNotFoundException, UnsupportedEncodingException{
        f.delete();
        String te = null;
        for (int i = 0; i < entityData.size(); i++) {
            for (int j = 0; j < entityData.get(i).size(); j++) {
                if (entityData.get(i).get(j).equals(id)) {
                        entityData.get(i).set(2,attempts);

                }
            }

        }

        PrintWriter writer = new PrintWriter(f, "UTF-8");
        StringBuilder sb;
        writer.println("userName:password:loginAttempts:title:division:patient:id:nurse:doctor");
        for (int i = 0; i < entityData.size(); i++) {
            sb = new StringBuilder();
            for (int j = 0; j < entityData.get(i).size(); j++) {
                sb.append(":" + entityData.get(i).get(j));
            }
            sb.deleteCharAt(0);
            writer.println(sb);
        }
        writer.close();


    }

    public ArrayList<String[]> getAllUsers()throws IOException{
        BufferedReader br = new BufferedReader(new FileReader(f));
        br.readLine();
        String temp;
        while((temp = br.readLine()) != null){
            String[] parts = temp.split(":");
            all.add(parts);
        }
        return all;
    }

    public String getName(String id)throws IOException{
        BufferedReader br2 = new BufferedReader(new FileReader(f));
        String temp = null;
        String[] parts;
        int counter = 0;
        while (counter == 0) {
            temp = br2.readLine();
            parts = temp.split(":");
           if(parts[6].equals(id)){
               return parts[0];
           }
        }

        return null;
    }


    public String[] getRightUser(String username)throws IOException{
        BufferedReader br = new BufferedReader(new FileReader(f));
        String temp = null;
        int counter = 0;
        while (counter == 0) {
            temp = br.readLine();
            if(temp == null){
                return null;
            }

            if (username.equals(temp.substring(0, temp.indexOf(":")))) {
                counter = 1;
            }

        }

        String[] parts = temp.split(":");
        return parts;
    }

    public entity getSortOfEntity(String username) throws IOException {
        String[] parts = getRightUser(username);


        switch (parts[3]) {
            case "patient":
                entity patient = new patient(parts[0], new division(parts[4]), parts[6]);
                patient.addDoctorAndNurse(parts[7],parts[8]);
                return patient;
            case "nurse":
                entity nurse = new nurse(parts[0], new division(parts[4]), parts[6]);
                nurse.addPatient(getRightUser(parts[5])[6]);
                return nurse;
            case "doctor":
                entity doctor = new doctor(parts[0], new division(parts[4]), parts[6]);
                doctor.addPatient(getRightUser(parts[5])[6]);
                return doctor;
            case "government":
                entity government = new government(parts[0], new division(parts[4]), parts[6]);
                return government;
            default:
                break;
        }
            return null;

    }

        public Boolean IsPasswordCorrect(String username, String password)throws java.lang.IllegalArgumentException{
            PasswordHasher ph = new PasswordHasher();
            return ph.checkPassword(password,userNamePassword.get(username));
        }

}
