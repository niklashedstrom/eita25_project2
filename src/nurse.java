import java.util.ArrayList;

public class nurse extends entity {
    private final static String TITLE = "nurse";
    private String patientId;

    public nurse(String name, division div, String id) {
        super(name, div, id);

    }
    public String getTitle() {
        return TITLE;
    }

    public boolean isDoctorOrNurse(){
        return true;
    }
    public void addPatient(String patientId) {
        this.patientId = patientId;
    }
    public String getPatient() {
        return patientId;
    }

    public boolean hasPatient(String id) {
        return patientId.equals(id);

    }

}
