import java.io.*;
import java.util.ArrayList;


public class journalController {

    private BufferedReader br;
    private ArrayList<ArrayList> medRecords;
    private ArrayList<String> mr;
    private File f = new File("src/journal.txt");


    public journalController() throws IOException {
        medRecords = new ArrayList<>();
        mr = new ArrayList<>();
        getJournalData();
    }

    private void getJournalData() throws IOException {
        br = new BufferedReader(new FileReader(f));
        String all;
        br.readLine();
        while ((all = br.readLine()) != null) {
            ArrayList<String> strings = new ArrayList<>();
            mr.add(all);
            String[] parts = all.split(":");
            for (int p = 0; p < parts.length; p++) {
                strings.add(parts[p]);
            }
            medRecords.add(strings);
        }
    }

    public String getPatient(String id) {
        for (int i = 0; i < medRecords.size(); i++) {
            for (int j = 0; j < medRecords.get(i).size(); j++) {
                if (medRecords.get(i).get(j).equals(id)) {
                    return (String) medRecords.get(i).get(j);
                }
            }


        }
        return null;
    }

    public String writeToMedRecord(String id,String nr, String text)throws FileNotFoundException, UnsupportedEncodingException{

        if(nr.equals("1") || nr.equals("2") || nr.equals("3") || nr.equals("4")){
        f.delete();
        int counter = Integer.parseInt(nr);
        String te = null;
        for (int i = 0; i < medRecords.size(); i++) {
            for (int j = 0; j < medRecords.get(i).size(); j++) {
                if (medRecords.get(i).get(j).equals(id)) {
                    counter--;
                    if (counter == 0) {

                        medRecords.get(i).set(4, text);
                    }

                }
            }
        }

        }else{
            return " ";
        }


        PrintWriter writer = new PrintWriter(f, "UTF-8");
        StringBuilder sb;
        writer.println("Patient:Id:Nurse:Doctor:MedicalRecord");
        for (int i = 0; i < medRecords.size(); i++) {
            sb = new StringBuilder();
            for (int j = 0; j < medRecords.get(i).size(); j++) {
                sb.append(":" + medRecords.get(i).get(j));
            }
            sb.deleteCharAt(0);
            writer.println(sb);
        }
        writer.close();

        return "Patient " + id + " successfully edited with " + text;
    }

    public void removeMedRec(String id,String nr) throws FileNotFoundException, UnsupportedEncodingException {
        f.delete();
        int counter = Integer.parseInt(nr);
        for (int i = 0; i < medRecords.size(); i++) {
            for (int j = 0; j < medRecords.get(i).size(); j++) {
                if (medRecords.get(i).get(j).equals(id)) {
                    counter--;
                    if(counter == 0){
                        medRecords.remove(i);
                        break;
                    }


                }
            }
        }
        //File file = new File("journal.txt");
        PrintWriter writer = new PrintWriter(f, "UTF-8");
        StringBuilder sb;
        writer.println("Patient:Id:Nurse:Doctor:MedicalRecord");
        for (int i = 0; i < medRecords.size(); i++) {
            sb = new StringBuilder();
            for (int j = 0; j < medRecords.get(i).size(); j++) {
                sb.append(":" + medRecords.get(i).get(j));
            }
            sb.deleteCharAt(0);
            writer.println(sb);
        }
        writer.close();

    }

    public String getMedRecord(String id) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < medRecords.size(); i++) {
            for (int j = 0; j < medRecords.get(i).size(); j++) {
                if (medRecords.get(i).get(j).equals(id)) {
                    sb.append(":"+medRecords.get(i).get(4));
                }
            }
        }
        sb.deleteCharAt(0);
        return sb.toString();
    }

    public void addMedRecord(String id,String text) throws FileNotFoundException, UnsupportedEncodingException {
        f.delete();
        ArrayList<String> array = new ArrayList<>();
        for (int i = 0; i < medRecords.size(); i++) {
            for (int j = 0; j < medRecords.get(i).size(); j++) {
                if (medRecords.get(i).get(j).equals(id)) {
                    for (int k = 0; k < medRecords.get(i).size() - 1; k++) {
                        array.add((String) medRecords.get(i).get(k));

                    }
                }

            }
        }
        array.add(text);
        medRecords.add(array);

        PrintWriter writer = new PrintWriter(f, "UTF-8");
        StringBuilder sb;
        writer.println("Patient:Id:Nurse:Doctor:MedicalRecord");
        for (int i = 0; i < medRecords.size(); i++) {
            sb = new StringBuilder();
            for (int j = 0; j < medRecords.get(i).size(); j++) {
                sb.append(":" + medRecords.get(i).get(j));
            }
            sb.deleteCharAt(0);
            writer.println(sb);
        }
        writer.close();

    }


    }


