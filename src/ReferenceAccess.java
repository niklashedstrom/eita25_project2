import javafx.scene.control.SplitPane;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class ReferenceAccess {
    journalController jc;
    userController uc;


    public ReferenceAccess() throws IOException {
        jc = new journalController();
        uc = new userController();


    }


    public String readAccess(entity ent, String patientId) throws IOException {
        String g = "government";
        if ((ent.isDoctorOrNurse() && ent.getPatient().equals(patientId)) ||
                (ent.isDoctorOrNurse() && checkSameDivPatients(ent.getDivision().toString()).contains(patientId) ) ||
                ent.getId().equals(patientId) || g.equals(ent.getTitle())) {

            StringBuilder sb = new StringBuilder();
            sb.append(patientId);
            sb.append(" ");
            sb.append(jc.getMedRecord(patientId));
            return sb.toString();
        }
        return " ";
    }

    private ArrayList<String> checkSameDivPatients(String div)throws IOException{
        ArrayList<String> divPatients = new ArrayList<>();
        ArrayList<String[]> all = uc.getAllUsers();
        for(int i = 0; i < all.size();i++){
            if(all.get(i)[4].equals(div)){
                divPatients.add(uc.getRightUser(all.get(i)[5])[6]);
            }
        }
        return divPatients;
    }




    public String writeAccess(entity ent,String patientId, String whichJournal, String text) throws FileNotFoundException, UnsupportedEncodingException {

        if(ent.isDoctorOrNurse()){
            if(patientId.equals(ent.getPatient())){
                return jc.writeToMedRecord(patientId,whichJournal,text);
            }
        }
        return " ";
    }
    public Boolean makeAccess(entity ent,String patientId,String text) throws FileNotFoundException, UnsupportedEncodingException {
        String d = "doctor";
        if (d.equals(ent.getTitle())) {
            if (patientId.equals(ent.getPatient())) {
                jc.addMedRecord(patientId,text);
                return true;
            }

        }
        return false;
    }



    public boolean deleteAccess(entity ent, String patientId,String nr) throws FileNotFoundException, UnsupportedEncodingException {
        String g = "government";
        if(g.equals(ent.getTitle())){
            if(patientId.equals(jc.getPatient(patientId))){
                jc.removeMedRec(patientId,nr);
                return true;
            }
        }else{
            return false;
        }
        return false;
    }
}
