
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;


public class TestLogger {
    private final Logger logger = Logger.getLogger(TestLogger.class
            .getName());
    private FileHandler fh = null;

    public TestLogger() {

        try {
            fh = new FileHandler("src/LogFile.log");
        } catch (Exception e) {
            e.printStackTrace();
        }

        fh.setFormatter(new SimpleFormatter());
        logger.addHandler(fh);
    }

    public void addToLog(String s) {
        logger.info(s);
    }
}