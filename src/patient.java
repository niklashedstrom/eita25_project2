public class patient extends entity{

        private final static String TITLE = "patient";
        private String doctor;
        private String nurse;

        public patient(String name, division div, String id) {
            super(name, div, id);

        }
        public String getTitle() {
            return TITLE;
        }

        public void addDoctorAndNurse(String doctor, String nurse){
            this.doctor = doctor;
            this.nurse = nurse;
        }

        public String getDoctor(){
            return doctor;
        }

        public String getNurse(){
            return nurse;
        }

}

