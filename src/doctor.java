import java.util.ArrayList;

public class doctor extends entity{


        private final static String TITLE = "doctor";
        private String patientId;

        public doctor(String name, division div, String id) {
            super(name, div, id);

        }
        public String getTitle() {
            return TITLE;
        }
        public boolean isDoctorOrNurse(){
            return true;
        }

        public String getPatient() {
            return patientId;
        }

        public boolean hasPatient(String id) {
            return patientId.equals(id);

        }

        public void addPatient(String patientId) {
               this.patientId = patientId;
        }


    }
