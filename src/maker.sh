#! /bin/bash

FIRST="$1"
TRUST=$FIRST"truststore"
KEY=$FIRST"keystore"

keytool -import -file CACert.pem -alias CA -keystore $TRUST -storepass password -noprompt

keytool -alias userkey -dname "CN=$FIRST" -genkeypair -keystore $KEY -storepass password -keypass password

keytool -alias userkey -certreq -file certSignReq.csr -keystore $KEY -storepass password

openssl x509 -CAcreateserial -in certSignReq.csr -req -CA CACert.pem -CAkey CAkey.pem -out certSignedReq -passin pass:password

keytool -import -file CACert.pem -alias CA -keystore $KEY -storepass password -noprompt
keytool -trustcacerts -alias userkey -import -file certSignedReq -keystore $KEY -storepass password

keytool -list -v -keystore $KEY -storepass password

rm *.csr CACert.srl CertSignedReq signedCertReq
