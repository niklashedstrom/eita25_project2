#! /bin/bash

openssl req -x509 -newkey rsa:2048 -keyout CAKey.pem -out CACert.pem -subj "/C=/ST=/L=/O=/OU=/CN=CA" -passout pass:password

keytool -alias keystore -dname "CN=myserver" -genkeypair -keystore serverkeystore -storepass password -keypass password

keytool -alias keystore -certreq -file serverCertReq.csr -keystore serverkeystore -storepass password

openssl x509 -CAcreateserial -in serverCertReq.csr -req -CA CACert.pem -CAkey CAkey.pem -out signedCertReq -passin pass:password

keytool -import -file CACert.pem -alias CA -keystore serverkeystore -storepass password -noprompt
keytool -trustcacerts -alias keystore -import -file signedCertReq -keystore serverkeystore -storepass password

keytool -list -v -keystore serverkeystore -storepass password

keytool -import -file CACert.pem -alias CA -keystore servertruststore -storepass password -noprompt

rm *.csr CACert.srl CertSignedReq signedCertReq
