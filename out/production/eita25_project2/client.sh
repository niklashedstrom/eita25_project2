#! /bin/bash


keytool -import -file CACert.pem -alias CA -keystore clienttruststore -storepass password -noprompt

keytool -alias userkey -dname "CN=Client" -genkeypair -keystore clientkeystore -storepass password -keypass password

keytool -alias userkey -certreq -file certSignReq.csr -keystore clientkeystore -storepass password

openssl x509 -CAcreateserial -in certSignReq.csr -req -CA CACert.pem -CAkey CAkey.pem -out certSignedReq -passin pass:password

keytool -import -file CACert.pem -alias CA -keystore clientkeystore -storepass password -noprompt
keytool -trustcacerts -alias userkey -import -file certSignedReq -keystore clientkeystore -storepass password

keytool -list -v -keystore clientkeystore -storepass password

rm *.csr CACert.srl CertSignedReq signedCertReq
